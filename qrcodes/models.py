from django.db import models


class QRImage(models.Model):

    pass


class QRCode(models.Model):
    owner = models.ForeignKey('wauth.WUser', on_delete=models.CASCADE, related_name='qrcodes')
    image = models.ImageField()
    address = models.CharField()



