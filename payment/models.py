from django.contrib.postgres.fields import JSONField
from django.db import models


class Payment(models.Model):

    PAYMENT_BID = 'bid'
    PAYMENT_SUBSCRIBE = 'subscribe'

    PAYMENT_TYPES = (
        (PAYMENT_BID, PAYMENT_BID),
        (PAYMENT_SUBSCRIBE, PAYMENT_SUBSCRIBE)
    )

    amount = models.DecimalField()
    bid = models.ForeignKey('bid.Bid', on_delete=models.CASCADE, related_name='payments')
    payment_type = models.CharField(max_length=30, choices=PAYMENT_TYPES)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


class Subscribe(models.Model):
    SUBSCRIBE_PENDING = 'pending'
    SUBSCRIBE_ACTIVE = 'active'
    SUBSCRIBE_CANCELED = 'canceled'
    SUBSCRIBE_PAUSED = 'paused'

    SUBSCRIBE_STATUSES = (
        (SUBSCRIBE_PENDING, SUBSCRIBE_PENDING),
        (SUBSCRIBE_ACTIVE, SUBSCRIBE_ACTIVE),
        (SUBSCRIBE_PAUSED, SUBSCRIBE_PAUSED),
        (SUBSCRIBE_CANCELED, SUBSCRIBE_CANCELED)
    )

    bid = models.OneToOneField('bid.Bid', on_delete=models.CASCADE, related_name='subscribe')
    frequency = JSONField(null=False, blank=True)
    status = models.CharField(max_length=50, choices=SUBSCRIBE_STATUSES)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

