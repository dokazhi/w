from django.contrib import auth
from rest_framework import serializers
from rest_framework import exceptions
from rest_framework.authtoken.models import Token

from wauth.models import WUser


class SignInSerializer(serializers.Serializer):
    login = serializers.CharField(required=True, write_only=True)
    password = serializers.CharField(required=True, write_only=True)
    message = serializers.CharField(read_only=True)
    token = serializers.CharField(read_only=True)

    def create(self, validated_data):
        user = WUser.objects.filter(username=validated_data.get('login')).first()

        if user.check_password(validated_data['password']):
            self.message = 'AUTHENTICATED'
            token, created = Token.objects.get_or_create(user=user)
            self.token = token
            return self
        else:
            raise exceptions.AuthenticationFailed


class SignUpSerializer(serializers.ModelSerializer):

    class Meta:
        model = WUser
        fields = '__all__'

    def create(self, validated_data):
        user = WUser.objects.create_user(**validated_data)
        return user
