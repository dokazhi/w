from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import Group


class WAUTHManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, email, password, user_type, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        username = self.model.normalize_username(username)
        user = self.model(username=username, email=email, user_type=user_type, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, email=None, password=None, user_type='customer', **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        user = self._create_user(username, email, password, user_type, **extra_fields)
        group = None
        if user_type == 'customer':
            group, created = Group.objects.get_or_create(name='customer')
        elif user_type == 'provider':
            group, created = Group.objects.get_or_create(name='provider')
        if group is not None:
            user.groups.add(group)
        else:
            raise ValueError('Bad group')
        return user

    def create_superuser(self, username, email, password, user_type='admin', **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, email, password, user_type, **extra_fields)
