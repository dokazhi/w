from django.contrib.auth.models import AbstractUser
from django.db import models

from wauth.managers import WAUTHManager


class WUser(AbstractUser):
    TYPE_CUSTOMER = 'customer'
    TYPE_PROVIDER = 'provider'
    TYPE_ADMIN = 'admin'
    TYPE_CHOICES = (
        (TYPE_CUSTOMER, TYPE_CUSTOMER),
        (TYPE_PROVIDER, TYPE_PROVIDER),
        (TYPE_ADMIN, TYPE_ADMIN)
    )
    phone = models.CharField(max_length=12, unique=True)
    user_type = models.CharField(max_length=50, choices=TYPE_CHOICES, default=TYPE_CUSTOMER)

    objects = WAUTHManager()
