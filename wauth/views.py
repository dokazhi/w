from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from marketplace.permissions import MarketplacePermissions
from wauth.serializers import SignInSerializer, SignUpSerializer


class SignInView(APIView):
    http_method_names = ('post', )
    permission_classes = (AllowAny, )

    def post(self, request):
        instance = SignInSerializer(data=request.data)
        if instance.is_valid(raise_exception=True):
            instance.save()

            return Response(instance.data, status=200)


class SignUpView(APIView):
    http_method_names = ('post', )
    permission_classes = (MarketplacePermissions, )

    def post(self, request):
        instance = SignUpSerializer(data=request.data)
        if instance.is_valid(raise_exception=True):
            instance.save()
            return Response(instance.data, status=200)
