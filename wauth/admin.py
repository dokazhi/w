from django.contrib import admin

# Register your models here.
from wauth.models import WUser


@admin.register(WUser)
class WUserAdmin(admin.ModelAdmin):
    pass