import hashlib
import time


def serve_product_image(instance, filename):
    owner_id = instance.product.owner.id
    product_id = instance.product.id
    extenstions = ('png', 'jpg', 'jpeg')
    extension = filename.split('.')[-1:]
    if extension in extenstions:
        hashy = hashlib.sha1()
        new_filename = hashy.update(str(time.time()).encode('utf-8'))
        return f'{owner_id}/{product_id}/{new_filename}.{extension}'
