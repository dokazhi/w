from django.db import models
from django_fsm import FSMField

from core import settings
from marketplace.utils import serve_product_image


class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)
    parent = models.ForeignKey(
        'marketplace.Category',
        related_name='childs',
        on_delete=models.SET_NULL,
        blank=True, null=True
    )
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


class ProductTag(models.Model):
    name = models.CharField(max_length=128, unique=True)
    tag = models.SlugField(max_length=128, unique=True)
    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


class Product(models.Model):
    STATUS_PENDING = 'pending'
    STATUS_DECLINED = 'declined'
    STATUS_ACTIVATED = 'activated'
    STATUS_DELETED = 'deleted'
    STATUS_ARCHIVED = 'archived'
    STATUS_CHOICES = (
        (STATUS_PENDING, STATUS_PENDING),
        (STATUS_DECLINED, STATUS_DECLINED),
        (STATUS_ACTIVATED, STATUS_ACTIVATED),
        (STATUS_DELETED, STATUS_DELETED),
        (STATUS_ARCHIVED, STATUS_ARCHIVED)
    )
    title = models.CharField(max_length=255, unique=True)
    short_description = models.CharField(max_length=255, blank=True)
    description = models.TextField(blank=True)
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    status = FSMField(default=STATUS_PENDING, choices=STATUS_CHOICES)
    category = models.ForeignKey(
        'marketplace.Category',
        on_delete=models.SET_NULL,
        related_name='products',
        null=True, blank=True
    )
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='products')

    tags = models.ManyToManyField('marketplace.ProductTag', related_name='products')

    modified = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)


class Image(models.Model):
    product = models.ForeignKey('marketplace.Product', on_delete=models.CASCADE, related_name='images')
    img = models.ImageField(upload_to=serve_product_image)
    is_main = models.BooleanField(default=False)
