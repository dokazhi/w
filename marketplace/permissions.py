from rest_framework import permissions
from rest_framework.authtoken.models import Token


class MarketplacePermissions(permissions.BasePermission):
    message = 'Unauthorized'

    def has_permission(self, request, view):
        token_str = request.META.get('HTTP_TOKEN')
        token = Token.objects.filter(key=token_str).first()
        print(token.user.products)
        return token_str is not None
