from rest_framework import viewsets, views, permissions

from marketplace.models import Product
from marketplace.serializers import ProductSerializer


class ProductView(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    permission_classes = (permissions.AllowAny, )
