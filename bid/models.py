import functools
import uuid
from decimal import Decimal

from django.db import models
from django_fsm import FSMField, transition

from core import settings


def save_and_refresh(method):
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        method(self, *args, **kwargs)
        self.save()
        return Bid.objects.get(id=self.id)

    return wrapper


class Bid(models.Model):
    BID_CREATED = 'created'
    BID_CANCELED = 'canceled'
    BID_PAID = 'paid'

    BID_CHOICES = (
        (BID_CREATED, BID_CREATED),
        (BID_PAID, BID_PAID),
        (BID_CANCELED, BID_CANCELED)
    )
    bid_uuid = models.UUIDField(max_length=128, primary_key=True, default=uuid.uuid4, editable=False)
    customer = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    status = FSMField(default=BID_CREATED, choices=BID_CHOICES)

    @save_and_refresh
    @transition(source=BID_CREATED, target=BID_PAID)
    def to_paid(self):
        pass

    @property
    def overall_bid_cost(self):
        result = 0
        for item in self.items.objects.all():
            result += item.item_cost
        return result


class BidItem(models.Model):
    bid = models.ForeignKey('bid.Bid', on_delete=models.CASCADE, related_name='items')
    product = models.ForeignKey('marketplace.Product', on_delete=models.CASCADE, related_name='bid_item')
    count = models.PositiveIntegerField()

    @property
    def item_cost(self):
        overall_cost = self.product.amount * self.count
        return Decimal(overall_cost)
